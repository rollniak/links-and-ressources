# links and ressources

List of link i have published in my blog


## learning/teaching way

- [Chasing 10X: Leveraging A Poor Memory In Engineering](https://senrigan.io/blog/chasing-10x-leveraging-a-poor-memory-in-software-engineering/)
- [Explaining Code using ASCII Art](https://blog.regehr.org/archives/1653)
- [[SRECon Keynote] So you want to be a wizard](https://jvns.ca/blog/so-you-want-to-be-a-wizard/)
- [The XY Problem](http://xyproblem.info/)
- [Teaching: A good (and bad) example](https://chrisdone.com/posts/teaching/)
- [https://learn code the hard way](https://learncodethehardway.org/)
- [Developper Roadmap](https://roadmap.sh/roadmaps)
- [De la bonne manière de poser les questions](https://www.gnurou.org/writing/smartquestionsfr/)
- [Online courses vs. colleges for software engineering](https://www.raahul.me/posts/online-courses-vs-colleges/)
- [Strategies, Tips, and Tricks for Anki](https://senrigan.io/blog/everything-i-know-strategies-tips-and-tricks-for-spaced-repetition-anki/)
- [Things to avoid with Anki](https://andrewzah.com/posts/2019/things-to-avoid-with-anki/)
- [Augmenting Long-term Memory](http://augmentingcognition.com/ltm.html)

- [Learn Anything](https://learn-anything.xyz/)
- [Get 1,500 free online courses from the world's leading universities --  Stanford, Yale, MIT, Harvard, Berkeley, Oxford and more.](http://www.openculture.com/freeonlinecourses)
- [MOOC CHATONS](https://mooc.chatons.org/)
- [Totem - Digital Security training for Activists & Journalists](https://learn.totem-project.org/)
- [La commune est a nous](https://la-commune-est-a-nous.commonspolis.org/)

## Writing/Bloging
### Articles
- [Long-form websites and typography](https://lawler.io/scrivings/long-form-websites-and-typography/)
- [Death by PowerPoint: the slide that killed seven people](https://mcdreeamiemusings.com/new-blog/2019/4/13/gsux1h6bnt8lqjd7w2t2mtvfg81uhx)
- [You should make a blog!](https://drewdevault.com/make-a-blog)
- [Journaling](https://jborichevskiy.com/posts/journaling/)
- [Stand out as a speaker](https://science.sciencemag.org/content/365/6455/834.full)
- [Text Rendering Hates You](https://gankra.github.io/blah/text-hates-you/)
- [MUST or MUST NOT on writing documentation](https://queue.acm.org/detail.cfm?id=3341158)

- [Weasyprint: The Awesome Document Factory](https://weasyprint.org/)

### Documentation
- [A beginner’s guide to writing documentation](https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/)
- [great-documentation tag on Jacob Kanplan-Moss blog](https://jacobian.org/tags/great-documentation/)
- [Producing Open Source Software](https://producingoss.com/en/index.html)
- [Réaliser une note de cadrage](https://www.manager-go.com/gestion-de-projet/dossiers-methodes/realiser-une-note-de-cadrage)
- [How to Write a Procedure: 13 Steps to Eclipse Your Competition ](https://www.process.st/how-to-write-a-procedure/)

### Communication

- [https://www.bortzmeyer.org/outils-bgp.html](https://www.honeycomb.io/blog/never-alone-on-call/)
- [La Communication Non-Violente rencontre Deadpool en milieu professionnel](https://blog.eleven-labs.com/fr/la-communication-non-violente-rencontre-deadpool-en-milieu-professionnel/)
- [Communication non violente dictionnaire et mise en pratique](http://www.cnvsuisse.ch/wp-content/uploads/2016/06/Codex-2.pdf)
- [Entretien utilisateur, mène ton enquête comme Benoit Blanc](https://blog.eleven-labs.com/fr/mener-entretien-utilisateur-benoit-blanc/)
- [Stand out as a speaker](https://science.sciencemag.org/content/365/6455/834.full)

### Tools
- [Commento, A fast, privacy-focused commenting platform](https://commento.io/)
- [Zola: Your one-stop static site engine](https://www.getzola.org/)

## Event

- [Write the Dosc](https://www.writethedocs.org/)
- [Config Management Camp](https://cfgmgmtcamp.eu)
- [Season of docs](https://developers.google.com/season-of-docs/)

## Les communs

- [Fondation P2P](https://wiki.p2pfoundation.net/French_language)
- [Le peer to peer : nouvelle formation sociale, nouveau modèle civilisationnel](https://wiki.p2pfoundation.net/Le_peer_to_peer_:_nouvelle_formation_sociale,_nouveau_mod%C3%A8le_civilisationnel)
- [TM2016 - Michel Bauwens](https://www.invidio.us/watch?v=ocHX_LH1jPU)

## Management

- [How do Individual Contributors Get Stuck?](https://medium.com/@skamille/how-do-individual-contributors-get-stuck-63102ba43516)
- [How do managers\* get stuck?](https://www.elidedbranches.com/2017/09/how-do-managers-get-stuck.html?m=1)
- [Rethink hiring: how automation helps tech startups get ahead](https://adamdrake.com/rethinking-hiring.html)
- [Filtering for better tech hiring](https://adamdrake.com/filtering-for-better-tech-hiring.html)
- [Writing the job description](https://adamdrake.com/writing-the-job-description.html)
- [Three Things Your People Need To Know](https://adamdrake.com/three-things-your-people-need-to-know.html)
- [Speak with Confidence](https://adamdrake.com/speak-with-confidence.html)
- [Développement et estimation de temps](https://mcorbin.fr/posts/2020-08-08-estimations/)
- [A guide to distributed teams](https://increment.com/teams/a-guide-to-distributed-teams/)
- [Problems, not solutions](https://ben.balter.com/2018/07/16/problems-not-solutions/)
- [On being an Engineering Manager](https://nickmchardy.com/2019/02/on-being-an-engineering-manager.html)
- [How to Deliver Constructive Feedback in Difficult Situations](https://medium.com/s/please-advise/the-essential-guide-to-difficult-conversations-41f736e63ccf)
- [You Should Organize a Study Group/Book Club/Online Group/Event! Tips on How to Do It](https://stephaniehurlburt.com/blog/2019/3/27/you-should-organize-a-study-groupbook-clubonline-groupevent-tips-on-how-to-do-it)
- [Resources to performe a portsmortems after incedents](https://postmortems.pagerduty.com/)
- [PagerDuty's Public Postmortem Documentation](https://github.com/PagerDuty/postmortem-docs)
- [hostedgraphite incident postmortem template](https://www.hostedgraphite.com/blog/incident-postmortem-template)
- [PagerDuty's Incident Response Documentation](https://github.com/PagerDuty/incident-response-docs)
- [Tests from the crypt](https://increment.com/testing/tests-from-the-crypt/)
- [Defining a Distinguished Engineer](https://blog.jessfraz.com/post/defining-a-distinguished-engineer/)
- [{Podcast} Building a Resilient Engineering Culture with Ryn Daniels](https://www.realworlddevops.com/episodes/building-a-resilient-engineering-culture-with-ryn-daniels)
- [{PDF} Busy person patterns](https://hillside.net/plop/2006/Papers/Library/PLoP%20Busy%20Person%20Pattern%20v8.pdf)
- [Taskwarrior, Free and Open Source Software that manages your TODO list from the command line](https://taskwarrior.org/)
- [How to Create a Runbook: A Guide for Sysadmins & MSPs](https://www.process.st/create-a-runbook/)
- [Standardizing Processes: How to Create a Documentation Style Guide](https://www.process.st/standardizing-processes/)
- [Thriving on the Technical Leadership Path ](https://keavy.com/work/thriving-on-the-technical-leadership-path/)
- [Operations and Internal Communication Strategies For Effective CEOs](https://www.sametab.com/blog/operations-and-internal-communication-strategies-for-effective-ceos)

## DevOps Culture

- [The Agile Fluency Model](https://martinfowler.com/articles/agileFluency.html)
- [Continuous Integration](https://martinfowler.com/articles/continuousIntegration.html)
- [Continuous Verification](https://www.verica.io/continuous-verification/)
- [Software Delivery Guide](https://martinfowler.com/delivery.html)
- [Test Driven Development](https://martinfowler.com/bliki/TestDrivenDevelopment.html)
- [reliability list](http://rachelbythebay.com/w/2019/07/21/reliability/)
- [Ironies of automation](https://blog.acolyer.org/2020/01/08/ironies-of-automation/)

- [Simple Sourcehut Deployments](https://sidhion.com/blog/posts/simple-sourcehut-deployments/)

## Chaos Infrastructure

- [Optimizing Business Response When Technical Incidents Happen](https://www.pagerduty.com/blog/business-response-ops-guides/)


### Tools
- [Capistrano - A remote server automation and deployment tool written in Ruby.](https://capistranorb.com/)

## Hardware

- [Raptor Computing system, the most Open source computer (Power 9 CPU based)](https://secure.raptorcs.com/)
- [Gemini PDA](https://store.planetcom.co.uk/products/gemini-pda-1?variant=6331545616411)
- [GL.iNet](https://docs.gl-inet.com/en/3/)
- [The Open Book Project, an open hardware ebook reader](https://github.com/joeycastillo/The-Open-Book)
- [Freetserv - An open-source hardware build-it-yourself device to remote-control up to 48 devices via their RS-232 serial port.](https://freetserv.github.io/)
- [Home Lab Beginners guide – Hardware](https://haydenjames.io/home-lab-beginners-guide-hardware/)

## Sysadmin

- [The Operations Report Card](http://opsreportcard.com/)
- [#25 - Bits Sysadmins Should Know](https://sysadmincasts.com/episodes/25-bits-sysadmins-should-know#planning)
- [How To Secure A Linux Server](https://github.com/imthenachoman/How-To-Secure-A-Linux-Server)
- [Cipherli.st Strong Ciphers for Apache, nginx and Lighttpd](https://cipherli.st/)
- [How to take back control of /etc/resolv.conf on Linux](https://www.ctrl.blog/entry/resolvconf-tutorial.html)

### FreeBSD
- [Adjusting acpi_video brightness increments on FreeBSD](https://www.davidschlachter.com/misc/freebsd-acpi_video-thinkpad-display-brightness)
- [Using a webcam on FreeBSD in web browsers](https://www.davidschlachter.com/misc/freebsd-webcam-browser)
- [Périphériques audio USB sur FreeBSD](https://www.davidschlachter.com/misc/freebsd-usb-audio.fr)
- [How I Created My First FreeBSD Port](https://aikchar.dev/blog/how-i-created-my-first-freebsd-port.html)
- [FreeBSD sur un ThinkPad - FreeBSD on a ThinkPad](https://adminblog.foucry.net/posts/info/freebsd-thinkpad/)
- [FreeBSD on ThinkPad x380](https://corrupted.io/2020/06/21/my-freebsd-laptop-build.html)
- [FreeBSD 12.0-RELEASE on the Lenovo Thinkpad T480](https://www.davidschlachter.com/misc/t480-freebsd)
- [How to manually install FreeBSD on a remote server (with UFS, ZFS, encryption...)](https://angristan.xyz/how-to-install-freebsd-server/)
- [WireGuard setup on FreeBSD](https://genneko.github.io/playing-with-bsd/networking/freebsd-wireguard-quicklook/)
- [Bastille, an open-source system for automating deployment and management of containerized applications on FreeBSD](https://bastillebsd.org/)
- [7 Days Challenge](https://wiki.freebsd.org/MichaelCrilly/7dayschallenge)
- [OpenBSD on OmniaOS Bhyve server](https://www.pbdigital.org/omniosce/bhyve/openbsd/2020/06/08/bhyve-zones-omnios.html)

### OpenBSD

- [Héberger son serveur avec OpenBSD](https://ybad.name/ah/)

### Container

#### Docker

- [How containers work: overlayfs](https://jvns.ca/blog/2019/11/18/how-containers-work--overlayfs/)
- [A review of the official Dockerfile best practices: good, bad, and insecure](https://pythonspeed.com/articles/official-docker-best-practices/)

- [DockerSlim](https://github.com/docker-slim/docker-slim) Don't change anything in your Docker container image and minify it by up to 30x (and for compiled languages even more) making it secure too!

#### Jails

- [Moving poudriere from the host into a jail](https://dan.langille.org/2019/10/23/moving-poudriere-from-the-host-into-a-jail/)
- [Installation de django-helpdesk dans une jail](https://adminblog.foucry.net/posts/info/helpdesk-jail/)
- [Setting up buildbot in FreeBSD jails](https://andidog.de/blog/2018-04-22-buildbot-setup-freebsd-jails)

### Network

- [Networking tool comics](https://jvns.ca/blog/2019/02/10/a-few-networking-tool-comics/)
- [Common misconceptions about IPv6 security](https://blog.apnic.net/2019/03/18/common-misconceptions-about-ipv6-security/)
- [Linux network performance parameters](https://github.com/leandromoreira/linux-network-performance-parameters)
- [5G Mobile Networks: A Systems Approach](https://5g.systemsapproach.org/index.html)
- [Représentation sous forme texte de ce qui passe sur le réseau](https://www.bortzmeyer.org/representation-texte.html)
- [Outils pour obtenir des informations BGP publiques](https://www.bortzmeyer.org/outils-bgp.html)
- [Un AS chez soi ? C'est possible !](https://blog.ataxya.net/un-as-chez-soi-cest-possible/)

- [Termshark,  a simple terminal user-interface for tshark](https://termshark.io/)

##### IPv6
- [ipv6.org](http://www.ipv6.org/) The starting point for everything about IPv6.
- [ipv6day.org](http://www.ipv6day.org/) All information needed to start your own IPv6 network.
- [ipv6 to standard](http://www.ipv6-to-standard.org/) The list of IPv6-enabled products.
- [ipv6 how to](http://www.bieringer.de/linux/IPv6/) Here, find the Linux IPv6-HOWTO and many links rela
ted to the topic.
- [RFC 2460](http://www.ietf.org/rfc/rfc2460.txt) The fundamental RFC about IPv6.
- [IP Time to Live (TTL) and Hop Limit Basics](https://packetpushers.net/ip-time-to-live-and-hop-limit-basics/)
- [Diving into the basics of IPv6](https://tunnelix.com/diving-into-the-basics-of-ipv6/)
- [RFC 8880: Special Use Domain Name 'ipv4only.arpa'](https://www.bortzmeyer.org/8880.html)

- [IPv6 health check](https://www.mythic-beasts.com/ipv6/health-check)

## Monitoring/Dashboard

- [Monitoring 101 : Que faut-il surveiller?](https://blog.devarieux.net/2017/11/monitoring-101-que-surveiller.html)
- [Monitoring 101 : Les alertes durant les astreintes](https://blog.devarieux.net/2017/10/monitoring-101-alertes-durant-astreintes.html)
- [Supervision répartie sur plusieurs sites avec Icinga](https://www.bortzmeyer.org/icinga-distributed.html)
- [The Monitoring Plugins Project](https://www.monitoring-plugins.org/)
- [The CASE Method: Better Monitoring For Humans](http://onemogin.com/monitoring/case-method-better-monitoring-for-humans.html)
- [Structure and Layout in System Dashboard Design](http://onemogin.com/observability/dashboards/practitioners-guide-to-system-dashboard-design.html)
- [Dashboard Organization and Naming](http://onemogin.com/observability/dashboards/dashboard-naming-and-organization.html)
- [Build an application monitoring stack with TimescaleDB, Telegraf & Grafana](https://blog.timescale.com/blog/build-an-application-monitoring-stack-with-timescaledb-telegraf-grafana/)
- [ciao - An Open Source HTTP monitoring Service](https://www.brotandgames.com/ciao/)
- [SmokePing keeps track of your network latency](https://oss.oetiker.ch/smokeping/)
- [[Tutoriel] Installer Prometheus/Grafana sans Docker](https://blog.zwindler.fr/2019/11/12/tutoriel-installer-prometheus-grafana-sans-docker/)
- [Journey into Observability: Telemetry](https://mads-hartmann.com/sre/2020/01/11/journey-into-observability-telemetry.html)

## Logs

- [Logging: Rules of thumb](https://engineering.hellofresh.com/logging-rules-of-thumb-f6c0f71a2351)
- [The Definitive Guide to Centralized Logging with Syslog on Linux](https://devconnected.com/the-definitive-guide-to-centralized-logging-with-syslog-on-linux/)

- [GoAccess is an open source real-time web log analyzer and interactive viewer that runs in a terminal in \*nix systems or through your browser.](https://goaccess.io/)

### services

#### DNS

- [DNS 101: An introduction to Domain Name Servers](https://www.redhat.com/sysadmin/dns-domain-name-servers)
- [A cartoon intro to DNS over HTTPS](https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/)
- [Mitigate DNS Infrastructure Tampering](https://cyber.dhs.gov/ed/19-01/)
- [DNS Security: Threat Modeling DNSSEC, DoT, and DoH](https://www.netmeister.org/blog/doh-dot-dnssec.html)
- [Unbound DNS Tutorial, Unbound DNS Tutorial](https://calomel.org/unbound_dns.html)
- [How to disable outgoing mDNS broadcasts on Linux](https://www.ctrl.blog/entry/how-to-disable-mdns-linux.html)
- [Blocking Ads using unbound(8) on OpenBSD](https://www.tumfatig.net/20190405/blocking-ads-using-unbound8-on-openbsd/)
- [Storing unbound(8) logs into InfluxDB](https://www.tumfatig.net/20190417/storing-unbound8-logs-into-influxdb/)
- [DNS Privacy Project](https://dnsprivacy.org/)
- [DNSSEC: DNS Security Extensions](https://www.dnssec.net/)
- [DNSCrypt](https://dnscrypt.info/)
- [Dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html)
- [OpenResolv](https://wiki.archlinux.org/index.php/Openresolv)
- [hblock](https://hblock.molinero.dev/)
- [Lexicon - Manipulate DNS records on various DNS providers in a standardized/agnostic way.](https://pypi.org/project/dns-lexicon/)
- [unwind-adblock - utiliser une blacklist avec le cache local unwind](https://prx.ybad.name/12.html)
- [Gérer un domaine local avec Unbound](https://www.shaftinc.fr/unbound-domaine-local.html)


#### Databases
- [he Theory of Relational Databases](http://web.cecs.pdx.edu/~maier/TheoryBook/TRD.html)
- [Let's Build a Simple Database, Writing a sqlite clone from scratch in C](https://cstack.github.io/db_tutorial/)
- [SQL queries don't start with SELECT](https://jvns.ca/blog/2019/10/03/sql-queries-don-t-start-with-select/)
- [10 Ways to Tweak Slow SQL Queries](http://www.helenanderson.co.nz/sql-query-tweaks/)
- [ORMs are backwards](https://abe-winter.github.io/2019/09/03/orms-backwards.html)
- [What ORMs have taught me: just learn SQL](https://wozniak.ca/blog/2014/08/03/What-ORMs-have-taught-me-just-learn-SQL/)

##### PostgreSQL

- [PostgreSQL Tutorial](https://www.postgresqltutorial.com/)
- [How Postgres Makes Transactions Atomic](https://brandur.org/postgres-atomicity)
- [PostgreSQL Features You May Not Have Tried But Should](https://pgdash.io/blog/postgres-features.html)
- [Vertically Scaling PostgreSQL](https://pgdash.io/blog/scaling-postgres.html)

## Nginx
- [Nginx Admin's Handbook](https://github.com/trimstray/nginx-admins-handbook)
- [Nginx Secure Web Server with HTTP, HTTPS SSL and Reverse Proxy Examples](https://calomel.org/nginx.html)
- [Public IP Address API with two lines of Nginx config](https://www.ecalamia.com/blog/show-ip-api-nginx/)

## programming/scripting/macro languages
### Shell

- [ Stupid Unix Tricks](https://sneak.berlin/20191011/stupid-unix-tricks/)
- [{Book} The Linux Command Line](http://linuxcommand.org/tlcl.php)
- [The Art of Command Line](https://github.com/jlevy/the-art-of-command-line)
- [The Bash Guide, A quality-driven guide through the shell's many features](https://guide.bash.academy/)
- [Pure Bash bible](https://github.com/dylanaraps/pure-bash-bible)
- [How Bash completion works](https://tuzz.tech/blog/how-bash-completion-works)
- [Explain Shell](https://explainshell.com/)
- [Awesome Bash, A curated list of delightful Bash scripts and resources.](https://github.com/awesome-lists/awesome-bash)
- [A surprisingly arcane little Unix shell pipeline example](https://utcc.utoronto.ca/~cks/space/blog/unix/ShellPipelineIndeterminate)
- [Things You Didn't Know About GNU Readline](https://twobithistory.org/2019/08/22/readline.html)
- [NuSehll, A Modern shell for the GitHub Era](http://www.nushell.sh/)
- [Rewritten in Rust: Modern Alternatives of Command-Line Tools](https://zaiste.net/posts/shell-commands-rust/)

### Rust
- [Cookin' with Rust](https://rust-lang-nursery.github.io/rust-cookbook/)
- [The Rust Programming Language](https://doc.rust-lang.org/book/)
- [Rust: au delà des types](https://blog.merigoux.ovh/fr/2019/04/16/verifier-rust.html)
- [Learning Parser Combinators With Rust](https://bodil.lol/parser-combinators/)
- [24 days of Rust](https://zsiciarz.github.io/24daysofrust/)
- [Command line apps in Rust](https://rust-lang-nursery.github.io/cli-wg/)
- [Secure Rust guide by ANSSI the National Cybersecurity Agency of France](https://anssi-fr.github.io/rust-guide/)
- [Diesel, a Safe, Extensible ORM and Query Builder for Rust](https://diesel.rs/)
- [Clap, A full featured, fast Command Line Argument Parser for Rust](https://github.com/clap-rs/clap)
- [Practical Networked Applications in Rust, Part 1: Non-Networked Key-Value Store](https://arveknudsen.com/posts/practical-networked-applications-in-rust/module-1/)
- [Rust Language Cheat Sheet](https://cheats.rs/)
- [Tera: A powerful, easy to use template engine for Rust](https://tera.netlify.com/)
- [Notes on Type Layouts and ABIs in Rust](https://gankra.github.io/blah/rust-layouts-and-abis/)
- [Error Handling Survey](https://blog.yoshuawuyts.com/error-handling-survey/)
- [Rewritten in Rust: Modern Alternatives of Command-Line Tools](https://zaiste.net/posts/shell-commands-rust/)

### C

- [Modern C](https://modernc.gforge.inria.fr/)
- [How to make your C codebase rusty: rewriting keyboard firmware keymap in Rust](https://about.houqp.me/posts/rusty-c/)

### Make
- [Makefiles, Best Practices](https://danyspin97.org/blog/makefiles-best-practices/)
- [The Language Agnostic, All-Purpose, Incredible, Makefile](https://blog.mindlessness.life/2019/11/17/the-language-agnostic-all-purpose-incredible-makefile.html)
- [Your Makefiles are wrong](https://tech.davis-hansson.com/p/make/)

### LaTeX

- [Gemini: A Modern LaTeX Poster Theme](https://www.anishathalye.com/2018/07/19/gemini-a-modern-beamerposter-theme/)
- [How I'm able to take notes in mathematics lectures using LaTeX and Vim](https://castel.dev/post/lecture-notes-1/)


## Mailing Lists
- [SecLists.Org Security Mailing List Archive](https://seclists.org/)
- [OpenSUSE Security Announce](http://lists.opensuse.org/opensuse-security-announce/)
- [Security Focus](http://www.securityfocus.com/)

## Mail server
- [Rspamd - Wiki Flat Tux](https://wiki.fiat-tux.fr/books/administration-syst%C3%A8mes/page/rspamd)
- [Tester son courrier électronique avec des auto-répondeurs](https://www.bortzmeyer.org/repondeurs-courrier-test.html)
- [MTA-STS is Hard. Here's how DNS Providers Can Make it Awesome With Automation...](https://www.agwa.name/blog/post/mta_sts_automation)
- [You should not run your mail server because mail is hard (Mail is not hard: people keep repeating that because they read it, not because they tried it)](https://poolp.org/posts/2019-08-30/you-should-not-run-your-mail-server-because-mail-is-hard/)
- [Setting up a mail server with OpenSMTPD, Dovecot and Rspamd](https://poolp.org/posts/2019-09-14/setting-up-a-mail-server-with-opensmtpd-dovecot-and-rspamd/)
- [Stop Validating Email Addresses With Regex](https://davidcel.is/posts/stop-validating-email-addresses-with-regex/)
- [How To Run Your Own Mail Server](https://www.c0ffee.net/blog/mail-server-guide/)

- [Email marketing regulations around the world](https://github.com/threeheartsdigital/email-marketing-regulations/blob/master/README.md)

## Tools

### cURL

- [Everything curl - the book](https://curl.haxx.se/book.html)
- [curl exercises](https://jvns.ca/blog/2019/08/27/curl-exercises/)
- [Convert curl syntax to Python, Ansible URI, Node.js, R, PHP, Strest, Go, Dart, JSON, Rust](https://curl.trillworks.com/)
- [You cannot cURL under pressure](https://blog.benjojo.co.uk/post/you-cant-curl-under-pressure)
- [Tooter en ligne de commande avec curl](https://prx.ybad.name/13.html)

### Shell
#### Articles
- [Better Bash history](https://sanctum.geek.nz/arabesque/better-bash-history/)
#### Outils
- [Spaceship ZSH](https://github.com/denysdovhan/spaceship-prompt)

## Python

- [Fundamentals of Python Programming](https://python.cs.southern.edu/pythonbook/pythonbook.pdf)
- [How to set up a perfect Python project](https://sourcery.ai/blog/python-best-practices/)
- [Python Logging: A Stroll Through the Source Code](https://realpython.com/python-logging-source-code/)
- [WTF Python](https://github.com/satwikkansal/wtfpython/blob/master/README.md)
- [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

- [Poetry, Python dependency management and packaging](https://github.com/python-poetry/poetry)
- [My Python Development Environment, 2020 Edition](https://jacobian.org/2019/nov/11/python-environment-2020/)

## Ruby

## Regex

- [Regex For Noobs (like me!) - an illustrated guide](https://www.janmeppe.com/blog/regex-for-noobs/)
- [RegExr is a HTML/JS based tool for creating, testing, and learning about Regular Expressions.](https://github.com/gskinner/regexr/)
- [Regex Cross­word](https://regexcrossword.com/)
- [Regex 101](https://regex101.com/)

### Git
- [My favourite Git commit](https://fatbusinessman.com/2019/my-favourite-git-commit)
- [Sourcehut, an open source software suite for managing your software development projects](https://sourcehut.org/)
- [Tips for a disciplined git workflow](https://drewdevault.com/2019/02/25/Using-git-with-discipline.html)
- [Oh shit, git!](https://ohshitgit.com/)
- [Oh Shit, git! Zine ver](https://gumroad.com/l/oh-shit-git)
- [Conventional Commits - A specification for adding human and machine readable meaning to commit messages](https://www.conventionalcommits.org/)
- [Commit messages guide](https://github.com/RomuloOliveira/commit-messages-guide)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
- [git rebase in depth](https://git-rebase.io/)

- [Git: une introduction](https://mcorbin.fr/posts/2020-06-26-git-introduction/)
- [Git: quelques commandes avancées.](https://mcorbin.fr/posts/2020-06-27-git-avancee/)
- [Git by exemple - A script with all Git explein in comment](https://bitbucket.org/BitPusher16/dotfiles/raw/49a01d929dcaebcca68bbb1859b4ac1aea93b073/refs/git/git_examples.sh)
- [git exercises: navigate a repository](https://jvns.ca/blog/2019/08/30/git-exercises--navigate-a-repository/)
- [Git Quick Stats](https://github.com/arzzen/git-quick-stats/)
- [Git blame someone else](https://github.com/jayphelps/git-blame-someone-else)

### Tmux
- [The Tao of tmux](https://leanpub.com/the-tao-of-tmux/read)

### Vim/NeoVim
- [Vim Is Saving Me Hours of Work When Writing Books & Courses](https://nickjanetakis.com/blog/vim-is-saving-me-hours-of-work-when-writing-books-and-courses)
- [Learn Vimscript the Hard Way](http://learnvimscriptthehardway.stevelosh.com/)
- [Fancy Vim Plugins](https://danyspin97.org/blog/fancy-vim-plugins/)
- [Vim and Git](https://vimways.org/2018/vim-and-git/)
- [At least one Vim trick you might not know](https://www.hillelwayne.com/post/intermediate-vim/)
- [Intermediate vim](https://dn.ht/intermediate-vim/)
- [Big Pile of Vim-like](https://vim.reversed.top/)
- [Vim-like Layer for Xorg and Wayland](https://cedaei.com/posts/vim-like-layer-for-xorg-wayland/)

- [Vim Awesome](https://vimawesome.com/)


## Courses
- [Hacker Toosl](https://hacker-tools.github.io)

## De-GAFAM-Ify
- [Google Groups Without Google Accounts](https://defn.io/2019/02/05/google-groups/)
- [Bye, Bye, Google](https://defn.io/2019/02/04/bye-bye-google/)

## Design

- [Easy to Remember Color Guide for Non-Designers](https://docs.sendwithses.com/random-stuff/easy-to-remember-color-guide-for-non-designers)
- [[PDF] Practical Rules for Using Color in Charts](https://www.perceptualedge.com/articles/visual_business_intelligence/rules_for_using_color.pdf)
- [Ikonate, fully customisable & accessible vector icons](https://www.ikonate.com/)
- [OpenEmoji - Open source emojis for designers, developers and everyone else!](https://openmoji.org/)
- [Woobro - Free Vector Images for Commercial Use](http://woobro.design/)
- [Faster Layouts with CSS Grid (and Subgrid!)](https://hacks.mozilla.org/2019/10/faster-layouts-with-css-grid-and-subgrid/)
- [Captain Icon](https://mariodelvalle.github.io/CaptainIconWeb/)

## Privacy

- [Privacy Is Just the First Step, the Goal Is Data Ownership](https://thetoolsweneed.com/privacy-is-just-the-first-step-the-goal-is-data-ownership/)
- [Why we moved our servers to Iceland](https://blog.simpleanalytics.io/why-we-moved-our-servers-to-iceland)

## Security

- [Pagerduty security training for everyone amd engineers](https://github.com/PagerDuty/security-training)
- [Using a Yubikey as smartcard for SSH public key authentication](https://www.undeadly.org/cgi?action=article;sid=20190302235509)
- [Scalable and secure access with SSH](https://engineering.fb.com/security/scalable-and-secure-access-with-ssh/)
- [Time for Password Expiration to Die](https://www.sans.org/security-awareness-training/blog/time-password-expiration-die)
- [Gérez vos mots de passe avec des logiciels libres avec git, gpg et pass](https://fr.jeffprod.com/blog/2019/gerez-vos-mots-de-passe-avec-des-logiciels-libres/)
- [How to use blacklistd(8) with NPF as a fail2ban replacement](https://www.unitedbsd.com/d/63-how-to-use-blacklistd8-with-npf-as-a-fail2ban-replacement)
- [Pros and cons of online assessment tools for web server security](https://infosec-handbook.eu/blog/online-assessment-tools/)

- [Unofficial Bitwarden compatible server written in Rust](https://github.com/dani-garcia/bitwarden_rs)
- [re:claimID](https://reclaim-identity.io/)
- [HashiCorp Vault : le cookbook](https://blog.wescale.fr/2019/09/11/vault-cookbook/)
- [The encrypted homelab - creating the most secure homelab there is](https://blog.haschek.at/2020/the-encrypted-homelab.html)

## Virtualization

- [Cockpit](https://cockpit-project.org/)

## Others tools
- [Chef InSpec is compliance as code](https://www.inspec.io/)
- [Packet Fence, an open source network access control](https://packetfence.org/)


## X/Wayland
- [Wayland misconceptions debunked](https://drewdevault.com/2019/02/10/Wayland-misconceptions-debunked.html)

## list of list

- [List of websites and whether or not they support 2FA.](https://twofactorauth.org/)
- [List of network services and web applications which can be hosted locally](https://github.com/Kickball/awesome-selfhosted)
- [A collection of inspiring lists, manuals, cheatsheets, blogs, hacks, one-liners, cli/web tools and more](https://github.com/trimstray/the-book-of-secret-knowledge)
- [Free For Dev, list of of services that have free tiers for developers](https://free-for.dev/)

## Miscellaneous

- [Unix as IDE: Introduction](https://sanctum.geek.nz/arabesque/series/unix-as-ide/)
- [How I'm still not using GUIs in 2019: A guide to the terminal](https://www.lucasfcosta.com/2019/02/10/terminal-guide-2019.html)
- [Linux Desktop Setup](https://hookrace.net/blog/linux-desktop-setup/)
- [Lorem Picsum, The Lorem Ipsum for photos](https://picsum.photos/)
- [Tallinn Manual 2.0, The most comprehensive guide for policy advisors and legal experts on how existing International Law applies to cyber operations](https://ccdcoe.org/research/tallinn-manual/)
- [HermiTux, A binary-compatible unikernel](https://ssrg-vt.github.io/hermitux/)
- [Ethical Alternatives & Resources](https://ethical.net/resources/)
- [Wizard Xines, a set of programming zines](https://wizardzines.com/)
- [SourcedFact, Journalism with Open Sourced Fact Checking](https://sourcedfact.com/)
- [OpenZFS, a fork of original ZFS file system](http://open-zfs.org/wiki/Main_Page)
- [ZFS Raidz Performance, Capacity and Integrity](https://calomel.org/zfs_raid_speed_capacity.html)
- [{Podcast} Heavy Networking 443: Architects Vs. Engineers – What’s The Difference?](https://packetpushers.net/podcast/heavy-networking-443-architects-vs-engineers-whats-the-difference/)
- [tildeverse.org](tildeverse.org)
- [VPN - a Very Precarious Narrative](https://schub.io/blog/2019/04/08/very-precarious-narrative.html)
- [Great developers are raised, not hired](https://sizovs.net/2019/04/10/the-best-developers-are-raised-not-hired/)
- [LD_PRELOAD: The Hero We Need and Deserve](https://blog.jessfraz.com/post/ld_preload/)
- [How to archive a page or an entire website in the Internet Archive](https://www.ctrl.blog/entry/how-to-internet-archive.html)
- [The software disenchantment](https://tonsky.me/blog/disenchantment/)
- [Get your RSS feeds as email](https://www.tumfatig.net/20190513/get-your-rss-feeds-as-email/)
- [Use RSS for Everything](https://limero.se/2019/07/30/use-rss-for-everything/)
- [Colonne d'identité et séquence](https://www.loxodata.com/post/identity/)
- [Recommandations de sécurité relatives à un système GNU/Linux](https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/)
- [How to setup a VPN server using WireGuard (with NAT and IPv6)](https://angristan.xyz/how-to-setup-vpn-server-wireguard-nat-ipv6/)
- [dn42 - The dynamic VPN](https://dn42.net/Home)
- [The Missing Wireguard Documentation](https://github.com/pirate/wireguard-docs)
- [list software of End of Life date](https://endoflife.date)
- [6.851: Advanced Data Structures (Fall'17)](https://courses.csail.mit.edu/6.851/fall17/)
- [Sec Alerts](https://secalerts.co/)
- [Failure is Familiar, Safety is Surprising](https://ryanfrantz.com/posts/failure-is-familiar-safety-is-surprising.html)
- [MicroG, A free-as-in-freedom re-implementation of Google’s proprietary Android user space apps and libraries.](https://microg.org/)
- [Awesome Static analysis](https://github.com/mre/awesome-static-analysis)
- [Black: The Uncompromising Code Formatter](https://github.com/python/black)
- [IPv6 test](https://ipv6-test.com/)
- [Human Panic; Panic messages for humans.](https://github.com/rust-cli/human-panic)
- [Chaos Engineering Traps](https://medium.com/@njones_18523/chaos-engineering-traps-e3486c526059)
- [Technical Debt in Software](https://martinfowler.com/bliki/TechnicalDebt.html)
- [Understanding the risk profile of your technical debt](https://content.pivotal.io/intersect/risk-profile-of-technical-debt)
- [Things I Learnt from a Senior Software Engineer](https://neilkakkar.com/things-I-learnt-from-a-senior-dev.html)
- [Recording video tutorials with (almost) zero budget](http://jpetazzo.github.io/2019/03/28/recording-workshop-video-tutorial-training/)
- [All in One mathematics cheat sheet](https://ourway.keybase.pub/mathematics_cheat_sheet.pdf)
- [Matlab-Python-Julia Cheat sheet](https://cheatsheets.quantecon.org/)
- [On the Impact of Programming Languages on Code Quality](https://arxiv.org/pdf/1901.10220.pdf)
- [ Dark theme, A dark theme is a low-light UI that displays mostly dark surfaces.](https://material.io/design/color/dark-theme.html)
- [Understanding the Linux Virtual Memory Manager](https://www.kernel.org/doc/gorman/pdf/understand.pdf)
- [CPU.fail](https://cpu.fail)
- [To Message Bus or Not: Distributed Systems Design](https://www.netlify.com/blog/2017/03/02/to-message-bus-or-not-distributed-systems-design/)
- [How to be a Programmer: A Short, Comprehensive, and Personal Summary](http://refcnt.org/~sts/docs/various/HowToBeAProgrammer.html)
- [x86 and amd64 instruction reference](https://www.felixcloutier.com/x86/)
- [CS 61: Systems Programming and Machine Organization (2018)](https://cs61.seas.harvard.edu/site/2018/)
- [Teach Yourself Computer Science](https://teachyourselfcs.com/)
- [Dark theme, A theme is a low-light UI that displays mostly dark surfaces](https://material.io/design/color/dark-theme.html)
- [How to file a good bug report](https://codingnest.com/how-to-file-a-good-bug-report/)
- [How to use GPG with YubiKey (bonus: WSL)](https://codingnest.com/how-to-use-gpg-with-yubikey-wsl/)
- [The Magic SysRq Key](https://nickdrozd.github.io/2019/03/19/sysrq.html)
- [Falsehoods programmers believe about Unix time](https://alexwlchan.net/2019/05/falsehoods-programmers-believe-about-unix-time/)
- [Things you’re probably not using in Python 3 – but should](https://datawhatnow.com/things-you-are-probably-not-using-in-python-3-but-should/)
- [Conformité RGPD : comment informer les personnes et assurer la transparence ?](https://www.cnil.fr/fr/conformite-rgpd-information-des-personnes-et-transparence)
- [Recording video tutorials with (almost) zero budget](https://jpetazzo.github.io/2019/03/28/recording-workshop-video-tutorial-training/)
- [Notes on running production code](https://kgrz.io/notes-on-running-production-code.html)
- [Strategies, Tips, and Tricks for Anki](https://senrigan.io/blog/everything-i-know-strategies-tips-and-tricks-for-spaced-repetition-anki/)
- [How to do hard things](https://www.drmaciver.com/2019/05/how-to-do-hard-things/)
- [It's time to replace GIFs with AV1 video!](https://www.singhkays.com/blog/its-time-replace-gifs-with-av1-video/)
- [How Complex Systems Fail](https://web.mit.edu/2.75/resources/random/How%20Complex%20Systems%20Fail.pdf)
- [LDAP Tool Box project](https://ltb-project.org/doku.php)
- [Peering into the future of Resilience Engineering in Tech](https://willgallego.com/2019/04/17/peering-into-the-future-of-resilience-engineering-in-tech/)
- [Automating chaos experiments in production](https://blog.acolyer.org/2019/07/05/automating-chaos-experiments-in-production/)
- [ So You Want To Be a Pentester?](https://jhalon.github.io/becoming-a-pentester/)
- [Interneting Is Hard, a Friendly web development tutorials for complete beginners](https://internetingishard.com/)
- [EdgeDB combines the simplicity of a NoSQL database with relational model’s powerful querying, strictness, consistency, and performance.](https://edgedb.com/)
- [No More Ransom](https://www.nomoreransom.org/fr/index.html)
- [HTTP Security Headers - A Complete Guide](https://nullsweep.com/http-security-headers-a-complete-guide/)
- [Fastest Way to Load Data Into PostgreSQL Using Python](https://hakibenita.com/fast-load-data-python-postgresql)
- [Lenovo Bios Simulator](https://download.lenovo.com/bsco/index.html)
- [Seriously, stop using RSA](https://blog.trailofbits.com/2019/07/08/fuck-rsa/)
- [How to write idempotent Bash scripts](https://arslan.io/2019/07/03/how-to-write-idempotent-bash-scripts/)
- [How to Use User Mode Linux](https://christine.website/blog/howto-usermode-linux-2019-07-07)
- [10 tips for reviewing code you don’t like](https://developers.redhat.com/blog/2019/07/08/10-tips-for-reviewing-code-you-dont-like/)
- [How to do a code review](https://google.github.io/eng-practices/review/reviewer/)
- [It's Never Too Late to Be Successful and Happy](https://invincible.substack.com/p/its-never-too-late-to-be-successful)
- [There’s more than one way to write an IP address](https://ma.ttias.be/theres-more-than-one-way-to-write-an-ip-address/)
- [My experience on Wayland so far (Sway)](https://an3223.github.io/My-experience-on-Wayland-so-far-(Sway)/)
- [Living without the modern browser](https://an3223.github.io/Living-without-the-modern-browser/)
- [Clear is better than clever](https://dave.cheney.net/2019/07/09/clear-is-better-than-clever)
- [Let's Build a Compiler 01: Starting Out](https://generalproblem.net/lets_build_a_compiler/01-starting-out/)
- [What every computer science major should know ](http://matt.might.net/articles/what-cs-majors-should-know/)
- [Think in Math. Write in Code.](https://justinmeiners.github.io/think-in-math/)
- [Problems and Solutions for Spaced Repetition Software](https://rickcarlino.com/2019/03/03/problems-and-solutions-for-spaced-repetition-software-html.html)
- [Composing better emails](https://iridakos.com/how-to/2019/06/26/composing-better-emails.html)
- [A Programmer’s Regret: Neglecting Math at University](https://awalterschulze.github.io/blog/post/neglecting-math-at-university/)
- [Visual Information Theory](https://colah.github.io/posts/2015-09-Visual-Information/)
- [Use plaintext email](https://useplaintext.email/)
- [Don't ask if a monorepo is good for you – ask if you're good enough for a monorepo](https://yosefk.com/blog/dont-ask-if-a-monorepo-is-good-for-you-ask-if-youre-good-enough-for-a-monorepo.html)
- [Alacritty integration with Tmux](https://arslan.io/2018/02/05/gpu-accelerated-terminal-alacritty/)
- [Understanding Docker container escapes](https://blog.trailofbits.com/2019/07/19/understanding-docker-container-escapes/)
- [Operable Software](Operable Software)
- [Talk, then code](https://dave.cheney.net/2019/02/18/talk-then-code)
- [The Three Laws of TDD](http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd)
- [How does into Meditation](https://christine.website/blog/how-does-into-meditation-2017-12-10)
- [When Then Zen: Anapana](https://christine.website/blog/when-then-zen-anapana-2018-08-15)
- [Site to Site WireGuard: Part 1 - Names and Numbers](https://christine.website/blog/site-to-site-wireguard-part-1-2019-04-02)
- [Practical Kasmakfa](https://christine.website/blog/practical-kasmakfa-2019-04-21)
- [Mailing lists vs Github](https://begriffs.com/posts/2018-06-05-mailing-list-vs-github.html)
- [Actually, DMARC works fine with mailing lists](https://begriffs.com/posts/2018-09-18-dmarc-mailing-list.html)
- [Complete guide to running a mailing list](https://begriffs.com/posts/2018-10-15-complete-guide-mailing-list.html)
- [Browsing a remote git repository](https://begriffs.com/posts/2019-02-21-browsing-remote-git.html)
- [Le site des expressions régulières](http://expreg.com/)
- [Memory management in Python](https://rushter.com/blog/python-memory-managment/)
- [Mental models box, the building blocks of making better decisions](https://www.mentalmodelsbox.com/)
- [Death Note: L, Anonymity & Eluding Entropy](https://www.gwern.net/Death-Note-Anonymity)
- [https://jsomers.net/blog/speed-matters](https://jsomers.net/blog/speed-matters)
- [Facelift Your IT Resume to Get More Interviews](https://cvcompiler.com/blog/facelift-your-it-resume-to-get-more-interviews/)
- [GTFOBins](https://gtfobins.github.io/)
- [What has to happen with Unix virtual memory when you have no swap space](https://utcc.utoronto.ca/~cks/space/blog/unix/NoSwapConsequence)
- [The Cloud how to ovoid the shit and make your won + found some resources to do it right](https://txt.black/~jack/cloud.txt)
- [Don’t Underestimate Grep Based Code Scanning](https://littlemaninmyhead.wordpress.com/2019/08/04/dont-underestimate-grep-based-code-scanning/)
- [The evolution of checklists in {the Chris Siebenmann} work](https://utcc.utoronto.ca/~cks/space/blog/sysadmin/ChecklistEvolution)
- [Observability Crash Course](http://onemogin.com/observability/dashboards/observability-crash-course.html)
- [Découverte du CSS : L'espacement](https://www.grafikart.fr/tutoriels/espace-margin-css-1179)
- [Debunking the Stanford Prison Experiment](https://www.gwern.net/docs/psychology/2019-letexier.pdf)
- [A Readable Specification of TLS 1.3](https://davidwong.fr/tls13/)
- [Poetry, The Python packaging and dependency management made easy](https://poetry.eustace.io/)
- [Why Dependent Haskell is the Future of Software Development](https://serokell.io/blog/why-dependent-haskell)
- [Se créer un initramfs manuellement](https://lord.re/posts/176-creer-initramfs-manuellement/)
- [Inhumanity of Root Cause Analysis](https://www.verica.io/inhumanity-of-root-cause-analysis/)
- [Security and privacy on UNIX desktop](https://danyspin97.org/blog/security-and-privacy-on-unix-desktop/)
- [web.dev, Test and learn how to improve your website](https://web.dev/)
- [Gwern site design](https://www.gwern.net/About#design)
- [L'art d'identifier des problématiques et des solutions pertinentes grâce au Design Thinking](https://blog.eleven-labs.com/design-thinking/)
- [RFC 8621: The JSON Meta Application Protocol (JMAP) for Mail](https://www.bortzmeyer.org/8621.html)
- [Configurer Firefox afin d’optimiser les performances et laisser le moins de traces](https://www.kali-linux.fr/configuration/configurer-firefox-optimiser-securite-performances)
- [Effects of leaving the comfort zone](https://pascalprecht.github.io/posts/effects-of-leaving-the-comfort-zone)
- [Improving your Open Source experience](https://pascalprecht.github.io/posts/open-source-lessons-learned)
- [A Productivity Manifesto](https://pascalprecht.github.io/posts/a-productivity-manifesto)
- [Why I use Vim](https://pascalprecht.github.io/posts/why-i-use-vim)
- [Best Haskell Conferences to Attend](https://cloud.digitalocean.com/)
- [How to Survive Working Remotely](https://serokell.io/blog/survive-remote-work)
- [How to make remote a success](https://blog.bearer.sh/how-to-make-remote-a-success/)
- [Start your own ISP](https://startyourownisp.com/)
- [Highlights from Git 2.23](https://github.blog/2019-08-16-highlights-from-git-2-23/)
- [Tech review Handbook](https://github.com/yangshun/tech-interview-handbook/)
- [The Architecture of Open Source Applications](https://www.aosabook.org/en/index.html)
- [The Art of Unix Programming](https://arp242.net/the-art-of-unix-programming/)
- [Every productivity thought I've ever had, as concisely as possible](https://guzey.com/productivity/)
- [Sécurité informatique en pays hostile](https://cpu.dascritch.net/post/2019/09/19/Ex0113-S%C3%A9curit%C3%A9-informatique-en-pays-hostile)
- [EditorConfig, a tool to maintain consistent conding styles for multiple developpers working on the same project across vaious editors or IDEs](https://editorconfig.org/)
- [Continuous Verification of Friday Deploys](https://willgallego.com/2019/08/23/continuous-verification-of-friday-deploys/)
- [hledger, a  Robust plain text accounting.](https://hledger.org/)
- [GnuPG – Introduction & Cheat-Sheet](https://net-security.fr/security/gnupg-introduction-cheat-sheet/)
- [No plan survives being misquoted](https://lawler.io/scrivings/no-plan-survives-being-misquoted/)
- [Software Architecture Guide](https://martinfowler.com/architecture/)
- [Des documentaires sur le Libre, la vie privée, l’informatique ?](https://linuxfr.org/news/des-documentaires-sur-le-libre-la-vie-privee-l-informatique)
- [Everything an open source maintainer might need to know about open source licensing](https://ben.balter.com/2017/11/28/everything-an-open-source-maintainer-might-need-to-know-about-open-source-licensing/)
 [Yes, No, Maybe, How to make a choice to add a feature](https://ben.balter.com/2018/05/04/yes-no-maybe/)
- [Distributed systems vocabulary.](https://lethain.com/distributed-systems-vocabulary/)
- [A Year of Working Remotely](https://mikeindustries.com/blog/archive/2019/08/a-year-of-working-remotely)
- [Why Bad Software Happens to Good People](https://www.csc.gov.sg/articles/how-to-build-good-software)
- [justdelete.me - A directory of direct links to delete your account from web services.](https://backgroundchecks.org/justdeleteme/)
- [Building interactive SSH applications](https://drewdevault.com/2019/09/02/Interactive-SSH-programs.html)
- [PySceneDetect is a command-line application and a Python library for detecting scene changes in videos, and automatically splitting the video into separate clips.](https://pyscenedetect.readthedocs.io/en/latest/)
- [Julia’s Release Process](https://julialang.org/blog/2019/08/release-process)
- [Bugtrackers - a special kind of hell](https://www.stefanschick.eu/posts/bugtrackers/)
- [ Mailing list ettiquette](https://man.sr.ht/lists.sr.ht/etiquette.md)
- [Beginner's Guide to Linkers](https://www.lurklurk.org/linkers/linkers.html)
- [Writing system software: code comments.](http://antirez.com/news/124)
- [I test in prod](https://increment.com/testing/i-test-in-production/)
- [Domotiser son espace de travail](https://blog.eleven-labs.com/fr/domotize-your-workspace/)
- [OpenBSD webserver with httpd, relayd and TLS](https://www.alexander-pluhar.de/openbsd-webserver.html)
- [NixNet](niixnet.xyz)
- [Programming Idioms, please don't reinvent the wheel](https://www.programming-idioms.org/)
- [Software Architecture is Overrated, Clear and Simple Design is Underrated](https://blog.pragmaticengineer.com/software-architecture-is-overrated/)
- [Filme un flic, sauve une vie ! Petit guide juridique pour filmer la police](https://paris-luttes.info/filme-un-flic-sauve-une-vie-petit-5966?lang=fr)
- [wtfismyip.com/](https://wtfismyip.com/)
- [Things You Didn't Know About GNU Readline ](https://twobithistory.org/2019/08/22/readline.html)
- [tanaguru contrast finder](https://contrast-finder.tanaguru.com/)
- [Journal d'un avocat - Mentions Légales](https://www.maitre-eolas.fr/pages/mentionslegales)
- [ethical, easy-to-use and privacy-conscious alternatives](https://switching.software/)
- [mitmproxy](https://mitmproxy.org/)
- [Kiss OS](https://getkiss.org/)
- [Carbon - Create and share beautiful images of your source code.](https://carbon.now.sh/)
- [Philosophy of Computer Science](https://cse.buffalo.edu/~rapaport/Papers/phics.pdf)
- [Uber Go style guide](https://github.com/uber-go/guide/blob/master/style.md)
- [Introduction to Theoretical Computer Science](https://introtcs.org/public/index.html)
- [The Tectonic Typesetting System](https://tectonic-typesetting.github.io/en-US/)
- [Rockbox is a free replacement firmware for digital music players.](https://www.rockbox.org/)
- [An Awk tutorial by Example](https://gregable.com/2010/09/why-you-should-know-just-little-awk.html)
- [No maintenance intended logo](http://unmaintained.tech/)
- [In the time you spend on social media each year, you could read 200 books](https://qz.com/895101/in-the-time-you-spend-on-social-media-each-year-you-could-read-200-books/)
- [The Unix Game is a fun, low-barrier-to-entry programming contest where players solve coding challenges by constructing "pipelines" of UNIX text processing utilities to compute the solution](https://www.unixgame.io/unix50J)
- [{PDF} Structure and Interpretation of Computer Programs, Second Edition by Harold Abelson, Gerald Jay Sussman, and Julie Sussman.](https://opendocs.github.io/sicp/)
- [Music Theory for Musicians and Normal People](https://tobyrush.com/theorypages/index.html)
- [Digital Forensics and Incident Response](https://www.jaiminton.com/cheatsheet/DFIR/)
- [Seren is a simple VoIP program based on the Opus codec that allows you to create a voice conference from the terminal](http://holdenc.altervista.org/seren/)
- [Is website vulnerable - finds publicly known security vulnerabilities in a website's frontend JavaScript libraries](https://github.com/lirantal/is-website-vulnerable)
- [Best Practices for Designing a Pragmatic RESTful API](https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api)
- [Building a BitTorrent client from the ground up in Go](https://blog.jse.li/posts/torrent/)
- [SSH Handshake Explained](https://gravitational.com/blog/ssh-handshake-explained/)
- [It’s Not Enough to Be Right—You Also Have to Be Kind](https://forge.medium.com/its-not-enough-to-be-right-you-also-have-to-be-kind-b8814111fe1)
- [Don't Call Yourself A Programmer, And Other Career Advice](https://www.kalzumeus.com/2011/10/28/dont-call-yourself-a-programmer/)
- [plemora](https://pleroma.social/)
- [What's remote like after 4.5 years?](https://wi.zard.work/posts/remote-musings/)
- [Comment se faire des amis sur Mastodon](https://www.cipherbliss.com/comment-se-faire-des-amis-sur-mastodon/)
- [FreeScout](https://github.com/freescout-helpdesk/freescout)
- [Home Assistant](https://www.home-assistant.io/)
- [ Your first ten customers ](https://stripe.com/en-ca/atlas/guides/starting-sales)
- [Slow Software](https://www.inkandswitch.com/slow-software.html)
- [Greatest hits from patio11](https://www.kalzumeus.com/greatest-hits/)
- [No such thing as a stupid question](https://en.wikipedia.org/wiki/No_such_thing_as_a_stupid_question)
- [Principle Of Least Astonishment](https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/freebsd-glossary.html#pola-glossary)
- [What nobody tells you about documentation](https://www.divio.com/blog/documentation/)
- [How to Create a Runbook: A Guide for Sysadmins & MSPs ](https://www.process.st/create-a-runbook/)
- [How to Write a Procedure: 13 Steps to Eclipse Your Competition](https://www.process.st/how-to-write-a-procedure/)
- [License professinnel, ADMINISTRATION DE SYSTÈMES, RÉSEAUX ET APPLICATIONS À BASE DE LOGICIELS LIBRES (ASRALL)](https://iut-charlemagne.univ-lorraine.fr/informatique/lp-informatique-asrall/)
- [Salary Negotiation: Make More Money, Be More Valued](https://www.kalzumeus.com/2012/01/23/salary-negotiation/)
- [Work Less, Get More Done: Analytics For Maximizing Productivity](https://www.kalzumeus.com/2009/10/04/work-smarter-not-harder/)
- [Building Highly Reliable Websites For Small Companies](https://www.kalzumeus.com/2010/04/20/building-highly-reliable-websites-for-small-companies/)
- [Code Health: Respectful Reviews == Useful Reviews ](https://testing.googleblog.com/2019/11/code-health-respectful-reviews-useful.html)
- [Seboss666 - Quelques astuces diverses, dix-septième](https://blog.seboss666.info/2019/11/quelques-astuces-diverses-dix-septieme/)
- [Understanding IOPS, Latency and Storage Performance](https://louwrentius.com/understanding-iops-latency-and-storage-performance.html)
- [Écris ta documentation technique bordel de merde](https://www.jesuisundev.com/ecris-ta-documentation-technique-bordel-de-merde/)
- [The Operations Report Card](http://opsreportcard.com/)
- [Linux Vendor Firmware Service](https://fwupd.org/)
- [HTMHell](https://www.htmhell.dev/)
- [Sketchviz](https://sketchviz.com/about)
- [XKCD-style plots in Matplotlib](https://jakevdp.github.io/blog/2012/10/07/xkcd-style-plots-in-matplotlib/)
- [ledger](https://www.ledger-cli.org/)
- [Grouce](https://gource.io/)
- [Broot](https://dystroy.org/broot/)
- [SIM is not secure](https://www.issms2fasecure.com/)
- [Local-first software: you own your data, in spite of the cloud](https://blog.acolyer.org/2019/11/20/local-first-software/)
- [Successfully Merging the Work of 1000+ Developers](https://engineering.shopify.com/blogs/engineering/successfully-merging-work-1000-developers)
- [Myths Programmers Believe about CPU Caches](https://software.rajivprab.com/2018/04/29/myths-programmers-believe-about-cpu-caches/)
- [Basic social skills guide](https://www.improveyoursocialskills.com/basic-social-skills-guide)
- [Nerd Fonts](https://www.nerdfonts.com/)
- [Reverse ingeneering for beginner](https://torus.company/writings/RE4B-EN.pdf)
- [Are we Wayland yet?](https://www.swalladge.net/archives/2019/10/14/are-we-wayland-yet/)
- [Latency/throughput tradeoffs, illustrated with coffee](https://medium.com/@kentbeck_7670/inefficient-efficiency-5b3ab5294791)
- [The First Non-Bullshit Book About Culture I’ve Read](https://zwischenzugs.com/2019/11/27/the-first-non-bullshit-book-about-culture-ive-read/)
- [Making Git and Jupyter Notebooks play nice](http://timstaley.co.uk/posts/making-git-and-jupyter-notebooks-play-nice/)
- [Almond - The Open, Privacy-Preserving Virtual Assistant](https://almond.stanford.edu/)
- [Yes, And – How to be effective in the theatre of work](https://tomcritchlow.com/2019/11/18/yes-and/)
- [Plain text project](https://plaintextproject.online/)
- [The real difference between CI and CD](https://fire.ci/blog/the-difference-between-ci-and-cd/)
- [My experience as a remote worker](https://www.joshwcomeau.com/career/remote-work)
- [Tract - Hugo theme for documentation project](https://tract-docs.dev/en/)
- [The sad state of sysadmin in the age of containers](https://www.vitavonni.de/blog/201503/2015031201-the-sad-state-of-sysadmin-in-the-age-of-containers.html)
- [A visual guide on troubleshooting Kubernetes deployments](https://learnk8s.io/troubleshooting-deployments)
- [Hugin - Create agents that monitor and act on your behalf. Your agents are standing by!](https://github.com/huginn/huginn)
- [Sécurisé SSH](https://www.tronyxworld.be/2020/hardening_ssh/)
- [Why making mistakes makes me a better sysadmin](https://opensource.com/article/20/8/sysadmin-mistakes)
- [Passive cooling and thermal management in data centers](https://ieeexplore.ieee.org/document/8211684)
- [encéphalomyélite myalgique (EM)](https://millionsmissing.fr/)
- [OpenMPTCProuter - OpenMPTCProuter use MultiPath TCP (MPTCP) to really aggregate multiple Internet connections and OpenWrt.](https://www.openmptcprouter.com/)
- [Quelles sont les mentions obligatoires sur un site internet ?](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31228)
- [Installer un cluster hyperconvergé avec Proxmox, Ceph, Tinc, OpenVSwitch](https://dryusdan.space/installer-un-cluster-proxmox-ceph-tinc-openvswitch/)
- [Ansible naming conventions](https://mcorbin.fr/posts/2018-05-12-ansible-naming/)
- [Introduction to eBPF and XDP](https://mcorbin.fr/posts/2019-03-03-ebpf-xdp-introduction/)
- [Un exemple d'infrastructure: gestion des images](https://mcorbin.fr/posts/2019-11-21-exemple-infra-vm-image/)
- [Un exemple d'infrastructure: création avec Terraform](https://mcorbin.fr/posts/2020-01-01-exemple-infra-terraform/)
- [Organiser un cours de langue](https://www.22decembre.eu/fr/2014/10/05/cours-lang/)
- [Guide GPG](Pourquoi vous devez utiliser GPG ?)
- [Jeedom & Grafana : marier la domotique et la métrologie](https://www.carmelo.fr/jeedom-grafana-marier-la-domotique-et-la-metrologie/)
- [Voir l'avancement de dd](https://prx.ybad.name/17.html)
- [THOREME - Andro-switch, pour une contraception naturelle & thermique dite masculine](https://www.thoreme.com/)
- [Filme un flic, sauve une vie ! Petit guide juridique pour filmer la police](https://paris-luttes.info/filme-un-flic-sauve-une-vie-petit-5966?lang=fr)
- [https://www.programming-idioms.org/](https://www.programming-idioms.org/)
- [ChocoPy is a programming language designed for classroom use in undergraduate compilers courses.](https://chocopy.org/)
- [China’s Maxim – Leave No Access Point Unexploited: The Hidden Story of China Telecom’s BGP Hijacking Story of China Telecom’s BGP Hijacking](https://scholarcommons.usf.edu/cgi/viewcontent.cgi?article=1050&context=mca)
- [Uber Go Style Guide](https://github.com/uber-go/guide/blob/master/style.md)
- [Sortir d'un /boot débordant](https://www.dadall.info/article728/sortir-dun-boot-debordant)
- [How to visualize RADIUS connections](https://blog.haschek.at/2016-how-to-visualize-radius-connections)
- [How to defend your website with ZIP bombs](https://blog.haschek.at/2017/how-to-defend-your-website-with-zip-bombs.html)
- [Build your own datacenter with PXE and Alpine](https://blog.haschek.at/2019/build-your-own-datacenter-with-pxe-and-alpine.html)
- [The encrypted homelab](https://blog.haschek.at/2020/the-encrypted-homelab.html)
- [Libre, as a service](https://blog.garambrogne.net/libre-as-a-service.html)
- [Bitwarden_rs - Wiki de Fiat- Tux](https://wiki.fiat-tux.fr/books/administration-syst%C3%A8mes/page/bitwarden_rs)
- [SysPass Password Management with LDAP support](https://doc.syspass.org/en/3.0/)
- [Instal­ler l’ex­ten­sion Netflix sur Kodi](https://fiat-tux.fr/2019/08/19/installer-lextension-netflix-sur-kodi/)
- [Pous­ser ses filtres SIEVE en ligne de commande et sans mot de passe (avec Kwal­let)](https://fiat-tux.fr/2019/10/21/pousser-ses-filtres-sieve-en-ligne-de-commande-et-sans-mot-de-passe-avec-kwallet/)
- [Tout ce que vous devez savoir sur la Network Time Security](https://www.internetsociety.org/fr/blog/2020/08/tout-ce-que-vous-devez-savoir-sur-la-network-time-security/)
- [Disable cron emails (solution)](https://haydenjames.io/disable-cron-emails/)
- [When debugging, your attitude matters](https://jvns.ca/blog/debugging-attitude-matters/)
- [Metaphors in man pages](https://jvns.ca/blog/2020/05/08/metaphors-in-man-pages/)
